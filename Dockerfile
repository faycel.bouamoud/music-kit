FROM node
RUN mkdir /app
WORKDIR /app
RUN apt-get update && apt-get install -y git && \
    git clone https://gitlab.com/faycel.bouamoud/music-kit.git /app &&\
    cd /app && npm install
EXPOSE 3000
ENTRYPOINT [ "npm", "start" ]