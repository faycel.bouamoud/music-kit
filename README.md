# SoundsGood MusicKit Search

Pour réaliser ce travail on a eu recours à react-create-app cli.
Ci-dessous le répertoire de projet, avec queleques information sur le role de chaque dossier.
## Arboresence du projet

├── public
└── src
    ├── components
    │   ├── header
    │   ├── music-search-form
    │   └── song
    ├── configurations
    └── services

## Architecture logique
Public: Contient la page index.html qui va ếtre lancer sur le navigateur.
src: Contient "la logique metier" de l'application
- Components: contient les composants réutilisables. tels que: Header, Fromualaire de recherche, etc.
- Configurations: Contient des configurations de quelques Api externes. dans notre cas: Apple MusicKit API
- Services: c'est la ou se trouve la consommation des webservices fourni par Apple Music API.

## Temps et réalisation 
Pour pouvoir réaliser ce petit projet:
- j'ai appris ReactJS: les fondamentaux. 5h (quelques rassemblences avec VueJS)
- ce petit projet est réalisé en 3h.

## Conclusion
J'éspère que j'ai bien réalisé ce qui est demandé. 
Je vous remercie vraiment pour me donner cette chance, et pour être un raison d'apprendre une nouvelle technologie 
et framework.
Etape suivante: Redux!


